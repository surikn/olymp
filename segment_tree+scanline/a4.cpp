#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <algorithm>
#define N 100013
using namespace std;

int a[N];
int min_tree[4 * N], max_tree[4 * N];

const int inf = 1e9;

void build(int id, int l, int r) {
	if (r - l == 1) {
		min_tree[id] = a[l];
		max_tree[id] = a[l];
		return;
	}
	int m = (l + r) / 2;
	build(id * 2 + 1, l, m);
	build(id * 2 + 2, m, r);
	min_tree[id] = min(min_tree[id * 2 + 1], min_tree[id * 2 + 2]);
	max_tree[id] = max(max_tree[id * 2 + 1], max_tree[id * 2 + 2]);
}

void update(int id, int l, int r, int i, int val) {
	if (r - l == 1) {
		min_tree[id] = val;
		max_tree[id] = val;
		return;
	}
	int m = (l + r) / 2;
	if (i < m) { // left son
		update(id * 2 + 1, l, m, i, val);
	} else {
		update(id * 2 + 2, m, r, i, val);
	}
	min_tree[id] = min(min_tree[id * 2 + 1], min_tree[id * 2 + 2]);
	max_tree[id] = max(max_tree[id * 2 + 1], max_tree[id * 2 + 2]);
}

pair <int, int> calc(int id, int l, int r, int sl, int sr) {
	if (sr <= l || r <= sl) {
		return { inf, -inf };
	}
	if (sl <= l && sr >= r) {
		return { min_tree[id], max_tree[id] };
	}
	int m = (l + r) / 2;
	auto left_v = calc(id * 2 + 1, l, m, sl, sr);
	auto right_v = calc(id * 2 + 2, m, r, sl, sr);
	return { min(left_v.first, right_v.first), max(left_v.second, right_v.second) };
}

int main() {
	int k;
	scanf("%d", &k);

	fill(min_tree, min_tree + N - 1, inf);
	fill(max_tree, max_tree + N - 1, -inf);

	for (int i = 0; i < N; i++) {
		a[i] = ((i % 12345) * (i % 12345)) % 12345 + (((i % 23456) * (i % 23456)) % 23456 * (i % 23456)) % 23456;
	}

	build(0, 0, N);

	int x, y;
	while (k--) {
		scanf("%d%d", &x, &y);
		if (x < 0) {
			update(0, 0, N, -x, y);
		}
		else {
			auto mm = calc(0, 0, N, x, y + 1);
			printf("%d\n", mm.second - mm.first);
		}
	}
}

