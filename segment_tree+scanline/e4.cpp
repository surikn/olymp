#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <algorithm>
#include <vector>
#include <iostream>
#define N 100013
using namespace std;

int a[N];
vector <int> max_tree(N * 4), prom(N * 4);

const int inf = 1e9;

void build(int id, int l, int r) {
	if (r - l == 1) {
		max_tree[id] = a[l];
		return;
	}
	int m = (l + r) / 2;
	build(id * 2 + 1, l, m);
	build(id * 2 + 2, m, r);
	max_tree[id] = max(max_tree[id * 2 + 1], max_tree[id * 2 + 2]);
}

void push(int id, int l, int r) {
	if (prom[id] == -inf) return;

	max_tree[id] += prom[id];
	if (r - l > 1) {
		if (prom[id * 2 + 1] == -inf) prom[id * 2 + 1] = prom[id];
		else prom[id * 2 + 1] += prom[id];
		if (prom[id * 2 + 2] == -inf) prom[id * 2 + 2] = prom[id];
		else prom[id * 2 + 2] += prom[id];
	}

	prom[id] = -inf;
}

int get_max(int id, int l, int r, int sl, int sr) {
	push(id, l, r);
	if (sr <= l || sl >= r) {
		return -inf;
	}
	if (sl <= l && sr >= r) {
		return max_tree[id];
	}
	int m = (l + r) / 2;
	return max(get_max(id * 2 + 1, l, m, sl, sr), get_max(id * 2 + 2, m, r, sl, sr));
}

void update_segment(int id, int l, int r, int sl, int sr, int d) {
	push(id, l, r);
	if (sr <= l || sl >= r) {
		return;
	}
	if (sl <= l && r <= sr) {
		if (prom[id] == -inf) prom[id] = d;
		else prom[id] += d;
		push(id, l, r);
		return;
	}
	int m = (l + r) / 2;
	update_segment(id * 2 + 1, l, m, sl, sr, d);
	update_segment(id * 2 + 2, m, r, sl, sr, d);
	max_tree[id] = max(max_tree[id * 2 + 1], max_tree[id * 2 + 2]);
}

int main() {
	// freopen("input.txt", "r", stdin);
	int n, m;
	cin >> n;

	for (int i = 0; i < n; i++) {
		cin >> a[i];
	}

	fill(max_tree.begin(), max_tree.end(), -inf);
	fill(prom.begin(), prom.end(), -inf);
	build(0, 0, n);

	cin >> m;

	char type;
	int l, r, val;
	while (m--) {
		cin >> type >> l >> r;
		if (type == 'm') {
			cout << get_max(0, 0, n, l - 1, r) << " ";
		}
		else {
			cin >> val;
			update_segment(0, 0, n, l - 1, r, val);
		}
	}
	return 0;
}
