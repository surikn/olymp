#include <iostream>
using namespace std;

typedef unsigned long long ll;

void solve();

int main() {
	ios::sync_with_stdio(false);
	cin.tie(0);
	cout.tie(0);

	solve();
}

ll merge(ll* arr, ll* aux, ll l, ll m, ll r) {
	ll k = l, i = l, j = m + 1;
	ll inversions = 0;

	while (i <= m && j <= r) {
		if (arr[i] <= arr[j]) {
			aux[k++] = arr[i++];
		}
		else {
			aux[k++] = arr[j++];
			inversions += (m - i + 1);
		}
	}

	while (i <= m) {
		aux[k++] = arr[i++];
	}
	for (int i = l; i <= r; i++) {
		arr[i] = aux[i];
	}

	return inversions;
}

ll merge_sort(ll* arr, ll* aux, int l, int r) {
	ll middle = (l + r) / 2, inversions = 0;
	if (r > l) {
		inversions += merge_sort(arr, aux, l, middle);
		inversions += merge_sort(arr, aux, middle + 1, r);
		inversions += merge(arr, aux, l, middle, r);
	}
	return inversions;
}

const int N = 600019;
ll arr[N], aux[N];

void solve() {
	ll n, l;
	cin >> n >> l;

	for (ll i = 0; i < n; i++) {
		cin >> arr[i];
		arr[i] = (ll)(arr[i] * (ll)l + (ll)i);
		aux[i] = arr[i];
	}

	cout << merge_sort(arr, aux, 0, n - 1) << "\n";
}
