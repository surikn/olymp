#include <iostream>
#include <algorithm>
using namespace std;

void solve();
typedef long long ll;

int main() {
	ios::sync_with_stdio(false);
	cin.tie(0);
	cout.tie(0);

	solve();
	return 0;
}

const int N = 100013;
const ll INF = 2147483647;

struct req {
	int l, r;
	ll q;
};
inline bool operator <(req& r1, req& r2) {
	return r1.q < r2.q;
}

req arr[N];
ll min_tree[4 * N], prom[4 * N];

void push(int id, int l, int r) {
	if (prom[id] == INF) return;
	min_tree[id] = prom[id];
	if (r - l > 1) {
		prom[id * 2 + 1] = prom[id];
		prom[id * 2 + 2] = prom[id];
	}
	prom[id] = INF;
}

void update(int id, int l, int r, int sl, int sr, ll v) {
	push(id, l, r);
	if (sr <= l || sl >= r) {
		return;
	}
	if (sl <= l && sr >= r) {
		prom[id] = v;
		push(id, l, r);
		return;
	}
	int m = (l + r) / 2;
	update(id * 2 + 1, l, m, sl, sr, v);
	update(id * 2 + 2, m, r, sl, sr, v);
	min_tree[id] = min(min_tree[id * 2 + 1], min_tree[id * 2 + 2]);
}

ll get_min(int id, int l, int r, int sl, int sr) {
	push(id, l, r);
	if (sr <= l || sl >= r) {
		return INF;
	}
	if (sl <= l && sr >= r) {
		return min_tree[id];
	}
	int m = (l + r) / 2;
	return min(get_min(id * 2 + 1, l, m, sl, sr), get_min(id * 2 + 2, m, r, sl, sr));
}

void solve() {
	int n, m;
	cin >> n >> m;

	for (int i = 0; i < m; i++) {
		cin >> arr[i].l >> arr[i].r >> arr[i].q;
		if (arr[i].l > arr[i].r) swap(arr[i].l, arr[i].r);
		arr[i].l--; arr[i].r--;
	}

	fill(min_tree, min_tree + n * 4, INF);
	fill(prom, prom + n * 4, INF);

	sort(arr, arr + m);

	for (int i = 0; i < m; i++) {
		update(0, 0, n, arr[i].l, arr[i].r + 1, arr[i].q);
	}

	for (int i = 0; i < m; i++) {
		if (get_min(0, 0, n, arr[i].l, arr[i].r + 1) != arr[i].q) {
			cout << "inconsistent\n";
			return;
		}
	}

	cout << "consistent\n";
	
	for (int i = 0; i < n; i++) {
		cout << get_min(0, 0, n, i, i + 1) << ' ';
	}

	cout << "\n";
}

