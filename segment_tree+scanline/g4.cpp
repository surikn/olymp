#include <iostream>
#include <vector>
#include <algorithm>
#define N 100000
using namespace std;

struct Point {
	int x, type, id;
};

vector <Point> points;
int res[N];

bool comp(Point p1, Point p2) {
	if (p1.x == p2.x)
		return p1.type > p2.type;
	return p1.x < p2.x;
}

int main() {
	ios_base::sync_with_stdio(0);
	cin.tie(0);
	cout.tie(0);

	int n, m;
	cin >> n >> m;

	int x1, x2;
	for (int i = 0; i < n; i++) {
		cin >> x1 >> x2;
		if (x1 > x2) swap(x1, x2);
		points.push_back({ x1, 1, -1 });
		points.push_back({ x2, -1, -1 });
	}

	for (int i = 0; i < m; i++) {
		cin >> x1;
		points.push_back({ x1, 0, i });
	}

	sort(points.begin(), points.end(), comp);

	int balance = 0; 
	for (auto p : points) {
		if (p.type == 0) {
			res[p.id] = balance;
		}
		balance += p.type;
	}

	for (int i = 0; i < m; i++) {
		cout << res[i] << " ";
	}
}
