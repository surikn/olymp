#include <iostream>
#include <vector>
#define N 20
using namespace std;
using Graph = vector< vector <int> >;

template <typename A, typename B>
bool mini(A& x, const B& y) {
	if (y < x) {
		x = y;
		return true;
	}
	return false;
}

template <typename A, typename B>
bool maxi(A& x, const B& y) {
	if (y > x) {
		x = y;
		return true;
	}
	return false;
}

bool gr[N][N];
int dp[(1 << N)];

bool bit_isset(int mask, int bit) {
	return (mask & (1 << bit));
}

void print_mask(int n, int bits = 32) {
	for (int i = bits - 1; i >= 0; --i) {
		cout << (n & (1 << i));
	}
}

int main() {
	ios_base::sync_with_stdio(false);
	cin.tie(NULL);

	int n;
	cin >> n;

	char conn;
	for (int i = 0; i < n; ++i) {
		for (int j = 0; j < n; ++j) {
			cin >> conn;
			if (conn == 'Y') {
				gr[i][j] = true;
				gr[j][i] = true;
			}
		}
	}

	for (int m = 1; m < (1 << n); ++m) {
		for (int v = 0; v < n; ++v) {
			for (int u = 0; u < v; ++u) {
				if (bit_isset(m, v) && bit_isset(m, u) && gr[u][v]) {
					int prev_mask = (m ^ (1 << v)) ^ (1 << u);
					maxi(dp[m], dp[prev_mask] + 2);
				}
			}
		}
	}

	cout << dp[(1 << n) - 1];
}
