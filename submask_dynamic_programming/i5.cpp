#include <iostream>
#include <math.h>
#include <algorithm>
#include <vector>

using namespace std;

const int N = 20;
const double INF = 1e9 + 7;     //?????????????

int gr[N][N];

double dp[1 << N][N];

int gb(int mask, int i) {
	return (mask >> i) & 1;
}

int sb(int mask, int i, int c) {
	return mask ^ ((c == gb(mask, i) ? 0 : 1) << i);
}

int main() {
	int n;
	cin >> n;

	vector<vector<int> > prev(1 << n, vector<int>(n, -1));

	for (int i = 0; i < n; i++) {
		for (int j = 0; j < n; j++) {
			cin >> gr[i][j];
		}
	}

	for (int mask = 0; mask < (1 << n); mask++) {
		for (int i = 0; i < n; i++) {
			dp[mask][i] = INF;
		}
	}

	dp[1][0] = 0;   

	for (int mask = 2; mask < (1 << n); mask++) {
		for (int i = 0; i < n; i++) {
			if ((mask >> i) & 1) {      
				int mask_without_i = mask ^ (1 << i);
				int cost = INF;
				int cur_prev = -1;

				for (int j = 0; j < n; j++) {
					if (j != i && ((mask >> j) & 1)) { 
						if (gr[j][i] != 0 && dp[mask_without_i][j] != INF && dp[mask_without_i][j] + gr[j][i] < cost) {
							cost = dp[mask_without_i][j] + gr[j][i];
							cur_prev = j;
						}
					}
				}

				if (cost < dp[mask][i]) {
					dp[mask][i] = cost;
					prev[mask][i] = cur_prev;
				}
			}
		}
	}

	double res = INF;
	int last = -1;
	int mask_all = (1 << n) - 1; 

	for (int i = 0; i < n; i++) {
		if (dp[(1 << n) - 1][i] < res) {
			last = i;
			res = dp[mask_all][i];
		}
	}

	if (res == INF) {
		cout << "-1\n";
		return 0;
	}

	vector<int> ans;
	int mask = (1 << n) - 1;
	while (mask != 0) {
		ans.push_back(last);
		int new_mask = sb(mask, last, 0);
		last = prev[mask][last];
		mask = new_mask;
	}
	reverse(ans.begin(), ans.end());

	cout << res << endl;
	for (int i = 0; i < ans.size(); ++i) {
		cout << ans[i] + 1 << " ";
	}
	cout << endl;

	return 0;
}
