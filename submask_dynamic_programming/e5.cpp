#include <iostream>
#define N 30
#define M 6
using namespace std;

int dp[N][(1 << M)];
bool possible[(1 << M)][(1 << M)];

bool bit_isset(int mask, int n) {
	return mask & (1 << n);
}

int main() {
	ios::sync_with_stdio(false);
	cin.tie(0);
	cout.tie(0);

	int n, m;
	cin >> n >> m;
	if (n < m) swap(n, m);

	for (int i = 0; i < (1 << m); ++i) {
		dp[0][i] = 1;
	}

	for (int m1 = 0; m1 < (1 << m); ++m1) {
		for (int m2 = 0; m2 < (1 << m); ++m2) {
			bool correct = true;
			for (int i = 1; i < m; ++i) {
				if ((bit_isset(m1, i) && bit_isset(m2, i) && bit_isset(m1, i - 1) && bit_isset(m2, i - 1)) ||
					(!bit_isset(m1, i) && !bit_isset(m2, i) && !bit_isset(m1, i - 1) && !bit_isset(m2, i - 1))) {
					correct = false;
					break;
				}
			}
			possible[m1][m2] = correct;
		}
	}

	for (int i = 1; i < n; ++i) {
		for (int prev_m = 0; prev_m < (1 << m); ++prev_m) {
			for (int curr_m = 0; curr_m < (1 << m); ++curr_m) {
				if (possible[prev_m][curr_m]) {
					dp[i][curr_m] += dp[i - 1][prev_m];
				}
			}
		}
	}

	int ans = 0;
	for (int i = 0; i < (1 << m); ++i) {
		ans += dp[n - 1][i];
	}

	cout << ans << "\n";
}
