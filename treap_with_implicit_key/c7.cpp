#include <iostream>
using namespace std;

typedef long long ll;

void solve();

int main() {
	ios::sync_with_stdio(false);
	cin.tie(0);
	cout.tie(0);
	solve();
}

const int N = 100000;

struct node {
	node* left, * right;
	int value, priority, size;
	ll sum;

	node(int n_value) {
		value = n_value;
		left = nullptr;
		right = nullptr;
		sum = n_value;
		size = 1;
		priority = (rand() << 15) | rand();
	}
};

int get_odd_l(int k) {
	return k / 2;
}

int get_odd_r(int k) {
	if (k % 2 != 0)
		return k / 2;
	return k / 2 - 1;
}

int get_even_l(int k) {
	if (k % 2 == 0) {
		return k / 2;
	}
	return k / 2 + 1;
}

int get_even_r(int k) {
	return k / 2;
}

int size(node* v) {
	return (v == nullptr ? 0 : v->size);
}

ll sum(node* v) {
	return (v == nullptr ? 0ll : v->sum);
}

void update(node* v) {
	if (v == nullptr) return;
	v->sum = sum(v->left) + sum(v->right) + (ll)v->value;
	v->size = size(v->left) + size(v->right) + 1;
}

node* merge(node* root1, node* root2) {
	if (root1 == nullptr) return root2;
	if (root2 == nullptr) return root1;
	if (root1->priority < root2->priority) {
		root1->right = merge(root1->right, root2);
		update(root1);
		return root1;
	} else {
		root2->left = merge(root1, root2->left);
		update(root2);
		return root2;
	}
}

pair <node*, node*> split(node* root, int k) {
	if (root == nullptr) return { nullptr, nullptr };
	if (size(root->left) >= k) {
		auto p = split(root->left, k);
		root->left = p.second;
		update(root);
		return { p.first, root };
	}
	else {
		auto p = split(root->right, k - size(root->left) - 1);
		root->right = p.first;
		update(root);
		return { root, p.second };
	}
}

ll get_sum(node* root, int l, int r) {
	auto p1 = split(root, r + 1);
	auto p2 = split(p1.first, l);
	ll ans = sum(p2.second);
	root = merge(p2.first, p2.second);
	root = merge(root, p1.second);
	return ans;
}

void write(node* root) {
	if (root == nullptr) return;
	write(root->left);
	cout << root->value << ' ' << root->sum << "->";
	write(root->right);
}

node* even_root = nullptr;
node* odd_root = nullptr;

void reverse(int odd_l, int odd_r, int even_l, int even_r) {
	auto odd_p1 = split(odd_root, odd_r + 1);
	auto odd_p2 = split(odd_p1.first, odd_l); // odd_p2.second

	auto even_p1 = split(even_root, even_r + 1);
	auto even_p2 = split(even_p1.first, even_l); // even_p2.second

	odd_root = merge(even_p2.second, odd_p1.second);
	odd_root = merge(odd_p2.first, odd_root);
	
	even_root = merge(odd_p2.second, even_p1.second);
	even_root = merge(even_p2.first, even_root);
}

void solve2() {
	while (true) {
		int l, r;
		cin >> l >> r;
		cout << "ODD TREE: " << get_odd_l(l) << ' ' << get_odd_r(r) << endl;
		cout << "EVEN TREE: " << get_even_l(l) << ' ' << get_even_r(r) << endl;
	}
}

void solve() {
	int n, m, id = 0;

	while (cin >> n >> m) {
		if (n == 0 && m == 0) return;

		id++;
		even_root = nullptr;
		odd_root = nullptr;
		int x;

		for (int i = 0; i < n; i++) {
			cin >> x;
			node* new_ver = new node(x);
			if (i & 1) {
				odd_root = merge(odd_root, new_ver);
			}
			else {
				even_root = merge(even_root, new_ver);
			}
		}

		if (id != 1) cout << "\n";
		cout << "Swapper " << id << ":\n";

		int type, a, b;
		while (m--) {
			cin >> type;
			if (type == 2) {
				cin >> a >> b; --a; --b;
				int odd_l = get_odd_l(a), odd_r = get_odd_r(b), even_l = get_even_l(a), even_r = get_even_r(b);
				ll even_sum = (even_l <= even_r ? get_sum(even_root, even_l, even_r) : 0ll);
				ll odd_sum = (odd_l <= odd_r ? get_sum(odd_root, odd_l, odd_r) : 0ll);
				cout << even_sum + odd_sum << "\n";
			}
			if (type == 1) {
				cin >> a >> b; --a; --b;
				int odd_l = get_odd_l(a), odd_r = get_odd_r(b), even_l = get_even_l(a), even_r = get_even_r(b);
				reverse(odd_l, odd_r, even_l, even_r);
			}
			if (type == 3) { // print odd and even trees
				cout << "ODD TREE: ";
				write(odd_root);
				cout << "\nEVEN TREE: ";
				write(even_root);
				cout << endl;
			}
			if (type == 4) {
				cin >> a >> b; --a; --b;
				int odd_l = get_odd_l(a), odd_r = get_odd_r(b), even_l = get_even_l(a), even_r = get_even_r(b);
				cout << "odd: " << odd_l << ' ' << odd_r << endl;
				cout << "even: " << even_l << ' ' << even_r << endl;
			}
		}
	}
}

