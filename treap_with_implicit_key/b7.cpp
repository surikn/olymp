#include <iostream>
#include <algorithm> 

using namespace std;

typedef long long ll;
typedef unsigned long long ull;


const int INF = 1e9;
void solve();

int main() {
	ios::sync_with_stdio(false);
	cin.tie(0);
	cout.tie(0);

	solve();
}

struct node {
	node *right, *left;
	int value, priority, size;
	int min;
	
	node(int n_value) {
		size = 1;
		value = n_value;
		min = n_value;
		right = nullptr;
		left = nullptr;
		priority = (rand() << 15) | rand();
	}
};

void write(node* root) {
	if (root == nullptr) return;
	write(root->left);
	cout << root->value << ' ';
	write(root->right);
}

int mini(node* v) {
	if (v == nullptr) return INF;
	return v->min;
}

int size(node* v) {
	if (v == nullptr) return 0;
	return v->size;
}

void update(node* root) {
	if (root == nullptr) return;
	root->size = size(root->left) + size(root->right) + 1;
	root->min = min(mini(root->left), min(mini(root->right), root->value));
}

node* merge(node* root1, node* root2) {
	if (root1 == nullptr) return root2;
	if (root2 == nullptr) return root1;
	if (root1->priority < root2->priority) { // new root is root1
		root1->right = merge(root1->right, root2);
		update(root1);
		return root1;
	} else {
		root2->left = merge(root1, root2->left);
		update(root2);
		return root2;
	}
}

pair <node*, node*> split(node* root, int k) {
	if (root == nullptr) return { nullptr, nullptr };
	if (size(root->left) >= k) {
		auto p = split(root->left, k);
		root->left = p.second;
		update(root);
		return { p.first, root };
	} else {
		auto p = split(root->right, k - size(root->left) - 1);
		root->right = p.first;
		update(root);
		return { root, p.second };
	}
}

node* root = nullptr;

int get_min(int l, int r) {
	auto p1 = split(root, r + 1);
	auto p2 = split(p1.first, l);
	int res = mini(p2.second);
	root = merge(p2.first, p2.second);
	root = merge(root, p1.second);
	return res;
}

void add_after(int k, int v) {
	auto p = split(root, k);
	auto new_node = new node(v);
	p.first = merge(p.first, new_node);
	root = merge(p.first, p.second);
}

void solve() {
	int n;
	cin >> n;

	char type; int a, b;
	while (n--) {
		cin >> type >> a >> b;
		if (type == '+') {
			add_after(a, b);
		} else if (type == '?') {
			cout << get_min(a - 1, b - 1) << '\n';
		} else if (type == 'w') {
			write(root);
			cout << endl;
		}
	}
}
