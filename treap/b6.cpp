#include <iostream>
#include <algorithm>
#include <vector>
#include <string>
using namespace std;

void solve();

int main() {
	ios::sync_with_stdio(false);
	cin.tie(0);
	cout.tie(0);

	solve();
}

// ************ Pretty code here **************

const int INF = 1e9;

// ************* DT basics *******************
struct node {
	int key, priority, size;
	node *left, *right;

	node(int n_key = 0) {
		size = 1;
		key = n_key;
		priority = ((rand() << 15) | rand());
		left = nullptr;
		right = nullptr;
	}
 };

int size(node* root) {
	return (root == nullptr ? 0 : root->size);
}

void upd(node* root) {
	if (root == nullptr) return;
	root->size = size(root->left) + size(root->right) + 1;
}

node* merge(node* root1, node* root2) {
	if (root1 == nullptr) {
		return root2;
	}
	if (root2 == nullptr) {
		return root1;
	}
	if (root1->priority < root2 ->priority) {
		root1->right = merge(root1->right, root2);
		upd(root1);
		return root1;
	}
	else {
		root2->left = merge(root1, root2->left);
		upd(root2);
		return root2;
	}
}

pair <node*, node*> split(node *root, int k) {
	if (root == nullptr) {
		return { nullptr, nullptr };
	}
	if (root->key < k) {
		auto p = split(root->right, k);
		root->right = p.first;
		upd(root);
		return { root, p.second };
	} else {
		auto p = split(root->left, k);
		root->left = p.second;
		upd(root);
		return { p.first, root };
	}
}

node* insert(node* root, node* v) {
	if (root == nullptr) {
		return v;
	}
	if (v == nullptr) {
		return root;
	}
	if (v->priority < root->priority) {
		auto p = split(root, v->key);
		v->left = p.first;
		v->right = p.second;
		upd(v);
		return v;
	}
	else {
		if (v->key < root->key) {
			root->left = insert(root->left, v);
		}
		else {
			root->right = insert(root->right, v);
		}
		upd(root);
		return root;
	}
}

pair <node*, node*> split_size(node *root, int k) {
	if (root == nullptr) {
		return { nullptr, nullptr };
	}
	if (size(root->left) >= k) {
		auto p = split_size(root->left, k);
		root->left = p.second;
		upd(root);
		return { p.first, root };
	} else {
		auto p = split_size(root->right, k - size(root->left) - 1);
		root->right = p.first;
		upd(root);
		return { root, p.second };
	}
}

bool find(node *root, int key) {
	if (root == nullptr) return false;
	if (root->key == key) return true;
	if (key < root->key) return find(root->left, key);
	else return find(root->right, key);
}

int find_kth(node *root, int id) {
	if (root == nullptr) {
		return -INF;
	}
	if (size(root->left) == id) {
		return root->key;
	}
	if (size(root->left) > id) {
		return find_kth(root->left, id);
	} else {
		return find_kth(root->right, id - size(root->left) - 1);
	}
}

int find_id(node* root, int key) { // lower bound
	if (root == nullptr) return 0;
	if (root->key == key) {
		return size(root->left);
	}
	if (key < root->key) {
		return find_id(root->left, key);
	}
	else {
		return find_id(root->right, key) + size(root->left) + 1;
	}
}

node* erase(node* root, int key) {
	if (root == nullptr) return nullptr;
	if (root->key == key) {
		return merge(root->left, root->right);
	}
	if (key < root->key) {
		root->left = erase(root->left, key);
	}
	else {
		root->right = erase(root->right, key);
	}
	upd(root);
	return root;
}

void write(node* root) {
	if (root == nullptr) return;
	write(root->left);
	cout << root->key << " ";
	write(root->right);
}
// ****************************************

int find_prev(node* root, int k, int res=-INF) {
	if (root == nullptr) return max(res, -INF);
	if (root->key >= k) {
		return find_prev(root->left, k, res);
	}
	else {
		res = max(res, root->key);
		if (root->right != nullptr) {
			return find_prev(root->right, k, res);
		}
		return res;
	}
}

node* root = nullptr;

void solve() {
	string req;
	int k;
	while (cin >> req >> k) {
		if (req == "insert") {
			node* v = new node(k);
			if (!find(root, k)) 
				root = insert(root, v);
		}
		if (req == "exists") {
			if (find(root, k)) {
				cout << "true\n";
			} else {
				cout << "false\n";
			}
		}
		if (req == "delete") {
			root = erase(root, k);
		}
		if (req == "kth") {
			int kth = find_kth(root, --k);
			if (kth == -INF) cout << "none\n";
			else cout << kth << "\n";
		}
		if (req == "next") {
			int next = find_kth(root, find_id(root, k + 1));
			if (next <= k) cout << "none\n";
			else cout << next << "\n";
		}
		if (req == "prev") { 
			int prev = find_prev(root, k);
			if (prev == -INF) cout << "none\n";
			else cout << prev << "\n";
		}
		if (req == "write") {
			write(root);
			cout << "\n";
		}
		if (req == "root") {
			cout << root->key << endl;
			if (root->left != nullptr) cout << "left: " << root->left->key << endl;
			if (root->right != nullptr) cout << "right: " << root->right->key << endl;
		}
	}
}

void solve2() {
	int n;
	cin >> n;

	string req;
	while (n--) {
		cin >> req;
		if (req == "add") {
			int key;
			cin >> key;
			node* v = new node(key);
			root = insert(root, v);
		}
		if (req == "erase") {
			int key;
			cin >> key;
			root = erase(root, key);
		}
		if (req == "write") {
			write(root);
			cout << "\n";
		}
		if (req == "kth") {
			int id;
			cin >> id;
			cout << find_kth(root, id) << "\n";
		}
		if (req == "number") {
			int key;
			cin >> key;
			cout << find_id(root, key) << "\n";
		}
	}
}
