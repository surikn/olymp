#include <vector>
#include <iostream>
#include <algorithm>

using namespace std;
const int inf = 10e9;

void solve();

int counter = 0;
vector<vector<int > > ans, inp;

struct node {
	int key, priority, index;
	node* left, * right;
	int size_;
	node(int n_key = 0, int priority_ = 0, int index_ = 0) {
		left = nullptr;
		right = nullptr;
		key = n_key;
		priority = priority_;
		index = index_;
		size_ = 1;
	}
};

int size(node* root) {
	if (root == nullptr) return 0;
	else return root->size_;
}

void upd(node* root) {
	if (root == nullptr) return;
	root->size_ = size(root->left) + size(root->right) + 1;
}

node* merge(node* root1, node* root2) {
	if (root1 == nullptr) return root2;
	if (root2 == nullptr) return root1;
	if (root1->priority < root2->priority) {
		root1->right = merge(root1->right, root2);
		upd(root1);
		return root1;
	}
	else {
		root2->left = merge(root1, root2->left);
		upd(root2);
		return root2;
	}
}

pair <node*, node*> split(node* root, int k) {
	if (root == nullptr) return { nullptr, nullptr };
	if (root->key < k) {
		pair <node*, node*> p = split(root->right, k);
		root->right = p.first;
		upd(root);
		return { root, p.second };
	}
	else {
		pair <node*, node*> p = split(root->left, k);
		root->left = p.second;
		upd(root);
		return { p.first, root };
	}
}

pair<node*, node*> split_by_size(node* root, int k) {
	if (root == nullptr) return { nullptr, nullptr };
	if (size(root->left) >= k) {
		pair<node*, node*> p = split_by_size(root->left, k);
		root->left = p.second;
		upd(root);
		return { p.first, root };
	}
	else {
		pair<node*, node*> p = split_by_size(root->right, k - size(root->left) - 1);
		root->right = p.first;
		upd(root);
		return { root, p.second };
	}
}

node* insert(node* root, node* v) {
	if (root == nullptr) return v;
	if (v == nullptr) return root;
	if (v->priority < root->priority) {
		pair <node*, node*> p = split(root, v->key);
		v->left = p.first;
		v->right = p.second;
		upd(v);
		return v;
	}
	else {
		if (v->key < root->key) {
			root->left = insert(root->left, v);
		}
		else {
			root->right = insert(root->right, v);
		}
		upd(root);
		return root;
	}
}

bool find(node* root, int key) {
	if (root == nullptr) return false;
	if (root->key == key) return true;
	if (key < root->key) return find(root->left, key);
	else return find(root->right, key);
}

node* erase_(node* root, int key) {
	if (root == nullptr) return nullptr;
	if (root->key == key) return merge(root->left, root->right);
	if (key < root->key) root->left = erase_(root->left, key);
	else root->right = erase_(root->right, key);
	upd(root);
	return root;
}

int find_kth(node* root, int id) {
	if (root == nullptr) return -inf;
	if (size(root->left) == id) return root->key;
	if (size(root->left) > id) return find_kth(root->left, id);
	else return find_kth(root->right, id - size(root->left) - 1);
}

int find_number(node* root, int key) {
	if (root == nullptr) return 0;
	if (root->key == key) return size(root->left);
	if (root->key > key) return find_number(root->left, key);
	else return find_number(root->right, key) + size(root->left) + 1;
}

void rez(node* root, int par = 0) {
	if (root == nullptr) return;
	ans[root->index - 1][0] = par;
	int l, r;
	root->left == nullptr ? l = 0 : l = root->left->index;
	root->right == nullptr ? r = 0 : r = root->right->index;
	ans[root->index - 1][1] = l;
	ans[root->index - 1][2] = r;

	rez(root->left, root->index);
	rez(root->right, root->index);
}

void write(node* root) {
	if (root == nullptr) return;
	write(root->left);
	cout << root->key << ' ';
	write(root->right);
}

node* root = nullptr;

signed main() {
	ios_base::sync_with_stdio(0);
	cin.tie(0); cout.tie(0);
	
	solve();
}

void solve() {
	int n;
	cin >> n;
	ans.assign(n, vector<int>(3));
	inp.assign(n, vector<int>(3));
	for (int i = 0; i < n; ++i) {
		cin >> inp[i][1] >> inp[i][0];
		inp[i][2] = i + 1;
	}
	sort(inp.rbegin(), inp.rend());

	for (int i = 0; i < n; ++i) {
		node* v = new node(inp[i][1], inp[i][0], inp[i][2]);
		root = insert(root, v);
	}
	rez(root);
	cout << "YES\n";
	for (auto p : ans) {
		cout << p[0] << ' ' << p[1] << ' ' << p[2] << "\n";
	}
}
