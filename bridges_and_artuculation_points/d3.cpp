#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <vector>
#include <algorithm>
#define N 200013
using namespace std;
using Graph = vector< vector <int> >;

Graph g(N);

bool used[N];
bool art_points[N];
int time_in[N], up[N];
int timer;

void dfs(int v, int p) {
	int child_cnt = 0;
	used[v] = true;
	time_in[v] = timer++;
	up[v] = time_in[v];

	for (auto u : g[v]) {
		if (u == p) continue;
		if (used[u]) {
			up[v] = min(up[v], time_in[u]);
		} else {
			child_cnt++;
			dfs(u, v);
			up[v] = min(up[v], up[u]);
			if (up[u] >= time_in[v] && p != -1) {
				art_points[v] = true;
			}
		}
	}
	if (p == -1 && child_cnt > 1) {
		art_points[v] = true;
	}
}

int main() {
	int n, m;
	scanf("%d%d", &n, &m);

	int h1, h2, h3;
	for (int i = 0; i < m; i++) {
		scanf("%d%d%d", &h1, &h2, &h3);
		g[n + i].push_back(h1 - 1);
		g[n + i].push_back(h2 - 1);
		g[n + i].push_back(h3 - 1);
		g[h1 - 1].push_back(n + i);
		g[h2 - 1].push_back(n + i);
		g[h3 - 1].push_back(n + i);
	}

	for (int i = 0; i < n + m; i++) {
		if (!used[i]) {
			dfs(i, -1);
		}
	}

	vector <int> ans;
	for (int i = 0; i < m; i++) {
		if (art_points[n + i]) {
			ans.push_back(i + 1);
		}
	}

	printf("%d\n", ans.size());
	for (int a : ans) {
		printf("%d\n", a);
	}
}
