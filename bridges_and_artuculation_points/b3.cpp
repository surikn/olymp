#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <vector>
#include <algorithm>
#define N 20013
using namespace std;

bool used[N], ans[N];
int time_in[N], time_out[N], up[N];
int timer;

vector< vector <int> >g(N);

void dfs(int v, int p) {
	int child_cnt = 0;
	used[v] = true;
	time_in[v] = timer++;
	up[v] = time_in[v];
	for (auto u : g[v]) {
		if (u == p) continue;
		if (used[u]) {
			up[v] = min(up[v], time_in[u]);
		} else {
			dfs(u, v);
			child_cnt++;
			up[v] = min(up[v], up[u]);
			if (up[u] >= time_in[v] && p != -1) {
				ans[v] = true;
			}
		}
	}
	if (p == -1 && child_cnt > 1) {
		ans[v] = true;
	}
}

int main() {
	int n, m;
	scanf("%d%d", &n, &m);

	int u, v;
	for (int i = 1; i <= m; i++) {
		scanf("%d%d", &u, &v);
		g[u].push_back(v);
		g[v].push_back(u);
	}

	for (int i = 1; i <= n; i++) {
		if (!used[i]) dfs(i, -1);
	}

	vector <int> out;

	for (int i = 1; i <= n; i++) {
		if (ans[i]) out.push_back(i);
	}
	printf("%d\n", out.size());
	for (int o : out) {
		printf("%d\n", o);
	}
}
