#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <vector>
#include <algorithm>
#include <set>
#define N 10113
using namespace std;

vector< vector <int> > g(N);
vector< vector <int> > r_g(N);

bool used[N];
pair<int, int> time_out[N];
int colours[N];
int timer = 1;

void calc_timeouts(int v) {
	used[v] = true;
	for (auto u : g[v]) {
		if (!used[u]) {
			calc_timeouts(u);
		}
	}
	time_out[v] = { timer++, v };
}

void colour_graph(int v, int colour) {
	used[v] = true;
	colours[v] = colour;
	for (auto u : r_g[v]) {
		if (!used[u]) {
			colour_graph(u, colour);
		}
	}
}

bool comp(pair <int, int> a, pair <int, int> b) {
	return a.first > b.first;
}

int main() {
	int n, m;
	scanf("%d%d", &n, &m);

	int u, v;
	for (int i = 1; i <= m; i++) {
		scanf("%d%d", &u, &v);
		g[u].push_back(v);
		r_g[v].push_back(u);
	}

	for (int i = 1; i <= n; i++) {
		if (!used[i]) {
			calc_timeouts(i);
		}
	}

	sort(time_out + 1, time_out + n + 1, comp);
	fill(used + 1, used + n + 1, 0);

	int colour = 0;
	for (int i = 1; i <= n; ++i) {
		if (!used[time_out[i].second]) {
			colour_graph(time_out[i].second, colour++);
		}
	}

	int ans = 0;

	set< pair<int, int> > res_edges;
	for (int i = 1; i <= n; ++i) {
		for (auto u : g[i]) {
			if (colours[i] != colours[u] && i != u) {
				res_edges.insert({ min(colours[i], colours[u]), max(colours[i], colours[u]) });
			}
		}
	}

	printf("%d\n", res_edges.size());
}
