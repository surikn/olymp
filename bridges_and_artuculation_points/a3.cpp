#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <algorithm>
#include <vector>
#define N 20000
using namespace std;

const int inf = 1e9;

bool used[N];
int time_in[N], time_out[N], up[N], timer = 0;
vector< vector < pair<int, int> > > g(N);

vector <int> ans;

void dfs(int v, int p) {
	used[v] = true;
	time_in[v] = timer++;
	up[v] = time_in[v];
	for (auto e : g[v]) {
		int u = e.first;
		if (u == p) continue;
		if (!used[u]) {
			dfs(u, v);
			up[v] = min(up[v], up[u]);
			if (up[u] == time_in[u]) { 
				ans.push_back(e.second);
			}
		} else {
			up[v] = min(up[v], time_in[u]);
		}
	}
	time_out[v] = timer++;
}

int main() {
	int n, m, u, v;
	scanf("%d%d", &n, &m);
	for (int i = 1; i <= m; i++) {
		scanf("%d%d", &u, &v);
		g[u].push_back({ v, i });
		g[v].push_back({ u, i });
	}

	for (int i = 1; i < n; i++) {
		if (!used[i]) dfs(i, -1);
	}

	sort(ans.begin(), ans.end());

	printf("%d\n", ans.size());
	for (int a : ans) {
		printf("%d\n", a);
	}
}
