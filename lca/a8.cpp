#include <iostream>
#include <vector>
using namespace std;
using Graph = vector< vector<int> >;

void solve();

int main() {
	ios::sync_with_stdio(false);
	cin.tie(0);
	cout.tie(0);

	solve();
}

// ******** My cool pretty code ************

const int N = 1e5+13;

Graph gr(N);
int time_in[N], time_out[N], depth[N];
int dp[N][30]; // 2**30
int timer = 0;

bool isParent(int u, int v) { // u is parent of v
	return (time_in[u] <= time_in[v] && time_out[u] >= time_out[v]);
}

int lca(int v, int u) {
	if (isParent(v, u)) return v;
	if (isParent(u, v)) return u;

	for (int i = 30 - 1; i >= 0; --i) {
		if (!isParent(dp[u][i], v)) {
			u = dp[u][i];
		}
	}

	return dp[u][0];
}

void dfs(int v, int par) { // call dfs(root, root);
	if (v != par)
		depth[v] = depth[par] + 1;
	time_in[v] = timer++;
	dp[v][0] = par;

	for (int i = 1; i < 30; ++i) {
		dp[v][i] = dp[dp[v][i - 1]][i - 1];
	}

	for (auto u : gr[v]) {
		if (u != par) {
			dfs(u, v);
		}
	}

	time_out[v] = timer++;
}

void solve() {
	int n, m;
	cin >> n;

	int u, v;	
	for (int i = 0; i < n - 1; i++) {
		cin >> u >> v;
		gr[--u].push_back(--v);
		gr[v].push_back(u);
	}

	dfs(0, 0);

	cin >> m;
	for (int i = 0; i < m; i++) {
		cin >> u >> v;
		int p = lca(--u, --v);
		cout << depth[u] + depth[v] - 2 * depth[p] << '\n';
	}
}
