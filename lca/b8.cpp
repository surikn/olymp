#include <iostream>
#include <algorithm>
#include <vector>
#include <string.h>

#pragma GCC optimize("Ofast,no-stack-protector") 
#pragma GCC target("sse,sse2,sse3,ssse3,sse4,popcnt,abm,mmx,avx,avx2,tune=native")
#pragma GCC optimize("unroll-loops") 
#pragma GCC optimize("fast-math") 
#pragma GCC optimize("profile-values,profile-reorder-functions,tracer") 
#pragma GCC optimize("rename-registers") 
#pragma GCC optimize("move-loop-invariants") 
#pragma GCC optimize("unswitch-loops") 
#pragma GCC optimize("function-sections") 
#pragma GCC optimize("data-sections") 
#pragma GCC optimize("branch-target-load-optimize") 
#pragma GCC optimize("branch-target-load-optimize2") 
#pragma GCC optimize("btr-bb-exclusive")

using namespace std;
using Graph = vector <vector <int> >;
typedef long long ll;

//char MEM[60 * 1024 * 1024];
//size_t PTR = 0;
//
//void* operator new(size_t s)
//{
//	char* ret = MEM + PTR + s;
//	PTR += s;
//	return ret;
//}
//
//void operator delete(void*)
//{
//
//}

void solve();

int main() {
	ios::sync_with_stdio(false);
	cin.tie(0);
	cout.tie(0);

	solve();
}

const int N = 100013;
const int K = 30; // logN

Graph gr(N);
int sparse_table[N][K + 1];
int depth[N];

void build_st(int n) {
	for (int i = 1; i < K; i++) {
		for (int node = 1; node <= n; node++) {
			if (sparse_table[node][i - 1] != -1) {
				sparse_table[node][i] = sparse_table[sparse_table[node][i - 1]][i - 1];
			}
		}
	}
}

void dfs(int v, int par) {
	depth[v] = depth[par] + 1;
	sparse_table[v][0] = par;
	for (auto u : gr[v]) {
		if (u != par) {
			dfs(u, v);
		}
	}
}

int lca(int u, int v) {
	if (depth[v] < depth[u])
		swap(u, v);
	int diff = depth[v] - depth[u];

	// ????????? ??????? ?? ??. ??????
	for (int i = 0; i < K; ++i) {
		if ((diff >> i) & 1) {
			v = sparse_table[v][i];
		}
	}

	if (u == v) return u;

	for (int i = K - 1; i >= 0; i--) {
		if (sparse_table[u][i] != sparse_table[v][i]) {
			u = sparse_table[u][i];
			v = sparse_table[v][i];
		}
	}

	return sparse_table[u][0];
}

int n;
ll x, y, z;

void solve() {
	int m;
	ll a1, a2, a3, a4, prev_q, ans = 0;
	cin >> n >> m;

	memset(sparse_table, -1, sizeof(sparse_table));

	int v;
	for (int i = 1; i < n; i++) {
		cin >> v;
		gr[i].push_back(v);
		gr[v].push_back(i);
	}
	cin >> a1 >> a2;
	cin >> x >> y >> z;

	dfs(0, 0);
	build_st(n);

	prev_q = lca(a1, a2);
	ans += prev_q;

	for (int i = 0; i < m - 1; i++) {
		a3 = (x * a1 + y * a2 + z) % n;
		a4 = (x * a2 + y * a3 + z) % n;
		a1 = a3; a2 = a4;
		prev_q = lca((a1 + prev_q) % n, a2);
		ans += prev_q;
	}

	cout << ans << "\n";
}
