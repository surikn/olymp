#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <algorithm>
#define N 1000
using namespace std;

int parent[N + N], sz[N + N];

struct Edge {
	int u, v, w;
};

Edge edges[N * N + N];

int get(int v) {
	if (parent[v] == v) {
		return v;
	}
	parent[v] = get(parent[v]);
	return parent[v];
}

int merge(int u, int v) {
	u = get(u); v = get(v);
	if (u == v) return 0;
	if (sz[u] > sz[v]) {
		swap(u, v);
	}
	parent[u] = v;
	sz[v] += sz[u];
	return 1;
}

bool comp(Edge e1, Edge e2) {
	return e1.w < e2.w;
}

int main() {
	int n;
	scanf("%d", &n);

	for (int i = 0; i < n + n; i++) {
		parent[i] = i;
		sz[i] = 1;
	}

	int cost, m = 0;
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < n; j++) {
			scanf("%d", &cost);
			if (j > i) {
				edges[m++] = { i, j, cost };
			}
		}
	}

	for (int i = 0; i < n; i++) {
		scanf("%d", &cost);
		edges[m++] = { n, i, cost };
	}

	sort(edges, edges + m, comp);

	long long int res = 0;
	for (int i = 0; i < m; i++) 
		if (merge(edges[i].u, edges[i].v)) res += edges[i].w;

	printf("%lld\n", res);
}
