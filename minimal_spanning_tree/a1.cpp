#define _CRT_SECURE_NO_WARNINGS
#pragma warning( disable : 6031 )

#include <stdio.h>
#include <vector>
#include <set>
#include <utility>
#define N 100001
using namespace std;

const int inf = 1e9;
typedef long long int ll;

struct edge {
	int to, w;

	edge(int _to, int _w) {
		to = _to;
		w = _w;
	}
};

inline bool operator <(const edge& e1, const edge& e2) {
	return e1.w < e2.w;
}

vector <vector <edge> > gr(N);
set < pair <int, int> > st;
bool used[N];
int dist[N];

int main() {
	int n, m;
	ll ans = 0;
	scanf("%d%d", &n, &m);

	for (int i = 0; i < n; i++) {
		dist[i] = inf;
	}

	int b, e, w;
	for (int i = 0; i < m; i++) {
		scanf("%d%d%d", &b, &e, &w);
		gr[--b].push_back(edge(--e, w));
		gr[e].push_back(edge(b, w));
	}

	st.insert({ 0, 0 });
	dist[0] = 0;
	while (!st.empty()) {
		int to = st.begin()->second;
		st.erase(st.begin());
		used[to] = true;

		for (auto e : gr[to]) {
			if (dist[e.to] > e.w && !used[e.to]) {
				st.erase({ dist[e.to], e.to });
				dist[e.to] = e.w;
				st.insert({ dist[e.to], e.to });
			}
		}
	}

	for (int d : dist) {
		ans += d;
	}

	printf("%lld\n", ans);
	return 0; 
}
