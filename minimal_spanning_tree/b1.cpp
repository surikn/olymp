#include <iostream>
#include <stdio.h>
#include <algorithm>
#include <vector>
#include <utility>
#include <set>
#include <tuple>
#define M 150001
using namespace std;

int parent[M], sz[M];

struct command {
	int u, v, type;
};

command coms[M];

int get(int v) {
	if (parent[v] == v) {
		return v;
	}
	parent[v] = get(parent[v]);
	return parent[v];
}

void merge(int u, int v) {
	u = get(u); v = get(v);
	if (u == v) return;
	if (sz[u] > sz[v]) {
		swap(u, v);
	}
	parent[u] = v;
	sz[v] += sz[u];
}

int main() {
	int n, m, k;
	cin >> n >> m >> k;

	for (int i = 1; i < n + 1; i++) {
		parent[i] = i;
	}

	int v, u;
	for (int i = 0; i < m; i++) {
		cin >> u >> v;
	}

	string type;
	for (int i = 0; i < k; i++) {
		cin >> type >> u >> v;
		if (type == "ask") {
			coms[i] = { u, v, 0 };
		} else {
			coms[i] = { u, v, 1 };
		}
	}

	reverse(coms, coms + k);

	vector <bool> answers;
	for (int i = 0; i < k; i++) {
		if (coms[i].type == 0) {
			answers.push_back(get(coms[i].u) == get(coms[i].v));
		} else {
			merge(coms[i].u, coms[i].v);
		}
	}

	reverse(answers.begin(), answers.end());
	for (bool a : answers) {
		cout << (a ? "YES\n" : "NO\n");
	}

	return 0;
}
