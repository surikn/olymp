#define _CRT_SECURE_NO_WARNINGS
#pragma warning( disable : 6031 )

#include <stdio.h>
#include <iostream>
#include <vector>
#include <set>
#include <utility>
#include <math.h>
#include <iomanip>
#define N 5001
using namespace std;

const int inf = 1e9;
typedef long long int ll;

pair<int, int> coords[N];
bool used[N];
double dist[N];

double calc_dis(pair <int, int> c1, pair <int, int> c2) {
	return sqrt((c1.first - c2.first)* (c1.first - c2.first) + (c1.second - c2.second) * (c1.second - c2.second));
}

int main() {
	int n, m;
	double ans = 0;
	scanf("%d", &n);

	for (int i = 0; i < n; i++) {
		dist[i] = inf;
	}

	int x, y;
	for (int i = 0; i < n; i++) {
		scanf("%d%d", &x, &y);
		coords[i] = { x, y };
	}

	dist[0] = 0.0;
	for (int i = 0; i < n; i++) {
		int v = -1;
		for (int j = 0; j < n; j++) {
			if (!used[j] && (v == -1 || dist[v] > dist[j])) {
				v = j;
			}
		}
		used[v] = true;
		ans += dist[v];
		for (int j = 0; j < n; j++) {
			double distance = calc_dis(coords[j], coords[v]);
			if (distance < dist[j]) {
				dist[j] = distance;
			}
		}
	}

	printf("%.9f\n", ans);
	return 0; 
}
