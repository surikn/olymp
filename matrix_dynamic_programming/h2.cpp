#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <vector>
#include <string.h>
#include <iostream>
using namespace std;

typedef long long int ll;
typedef unsigned long long int ull;

const ll mod = 1000000007;

void fill_zeros(vector< vector<ll> >& arr) {
	for (auto &v : arr) {
		fill(v.begin(), v.end(), 0);
	}
}

void print_mat(vector< vector<ll> >& arr) {
	int n = arr.size(), m = arr[0].size();
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < m; j++) {
			printf("%lld ", arr[i][j]);
		} printf("\n");
	}
}

void mul(vector< vector<ll> > &a, vector< vector<ll> > &b, vector< vector <ll> > &c) {
	/*if (a[0].size() != b.size()) {
		printf("Invalid data in mul arr(%d, %d)\n", a[0].size(), b.size());
		exit(228);
	}*/
	int n = a.size(), m = b.size(), k = b[0].size();
	for (int i = 0; i < n; ++i) {
		for (int j = 0; j < k; ++j) {
			for (int q = 0; q < m; ++q) {
				c[i][j] = (c[i][j] + (a[i][q] * b[q][j]) % mod) % mod;
			}
		}
	}
}

vector< vector <ll> > binpow(vector< vector<ll> > &arr, ll pow) {
	/*if (arr.size() != arr[0].size()) {
		printf("Invalid data in binpow arr(%d, %d)\n", arr.size(), arr[0].size());
		exit(47);
	}*/
	int n = arr.size();
	vector < vector <ll> > res(n, vector <ll>(n));
	if (pow == 0) {
		for (int i = 0; i < n; i++) {
			res[i][i] = 1;
		}
		return res;
	}
	if (pow % 2 == 1) {
		auto b = binpow(arr, pow - 1);
		mul(b, arr, res);
	} else {
		auto b = binpow(arr, pow / 2);
		int size1 = b.size(), size2 = b[0].size();
		mul(b, b, res);
	}
	return res;
}

ll sum(vector< vector<ll> > &arr) {
	int n = arr.size(), m = arr[0].size();
	ll res = 0;
	for (int i = 0; i < n; ++i) {
		for (int j = 0; j < m; ++j) {
			res = (res + arr[i][j]) % mod;
		}
	}
	return res;
}

int main() {
	int n, m, k;
	scanf("%d%d%d", &n, &m, &k);

	vector < vector<ll> > gr(n, vector<ll>(n));

	int u, v;
	for (int i = 0; i < m; i++) {
		scanf("%d%d", &u, &v);
		gr[u - 1][v - 1]++;
	}
	auto k_path = binpow(gr, k);

	ll res = 0;
	for (int i = 0; i < n; i++) {
		res = (res + k_path[0][i]) % mod;
	}

	printf("%lld\n", res);
	return 0;
}
