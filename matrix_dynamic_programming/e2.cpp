#include <iostream>
#include <string>
#define N 100
using namespace std;

typedef long long int ll;

const ll mod = 1e9 + 7;
ll dp[N][10];

bool chk(string s) {
	for (int i = 1; i < s.length(); i++) {
		if (s[i] < s[i - 1]) {
			return false;
		}
	}
	return true;
}

ll rec(int pos, string num) {
	int dp_pos = num.length() - pos - 1;
	if (dp_pos == -1) return 1;

	short numeral, prev_num;
	numeral = num[pos] - '0';
	prev_num = (pos == 0 ? 1 : num[pos - 1] - '0');

	if (prev_num > numeral) return 0;

	return ((dp[dp_pos][numeral - 1] - dp[dp_pos][prev_num - 1] + mod) % mod + rec(pos + 1, num)) % mod;
}

ll calc_beauty(string num) {
	int n = num.length();
	ll res = 0;
	for (int i = 0; i < n - 1; i++) {
		res = (res + dp[i][9]) % mod;
	}
	res = (res + rec(0, num)) % mod;
	return res;
}

int main() {
	string sl, sr;
	cin >> sl >> sr;

	bool sl_b = chk(sl);

	int l = sl.length(), r = sr.length();
	
	for (int i = 1; i < 10; i++) {
		dp[0][i] = dp[0][i - 1] + 1;
	}
	for (int i = 1; i <= r; i++) {
		for (int j = 1; j < 10; j++) {
			dp[i][j] = dp[i - 1][9] - dp[i - 1][j - 1];
			dp[i][j] = (dp[i][j] + mod) % mod;
			dp[i][j] = (dp[i][j] + dp[i][j - 1]) % mod;
		}
	}

	ll beauty_r = calc_beauty(sr);
	// printf("Beauty r: %lld\n", beauty_r); //DEBUG

	ll beauty_l = calc_beauty(sl);
	// printf("Beauty l: %lld\n", beauty_l); //DEBUG

	ll res = (beauty_r - beauty_l + mod) % mod + sl_b;
	printf("%lld", res);
}
