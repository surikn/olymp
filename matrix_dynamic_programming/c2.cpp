#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <vector>
#include <string.h>
#include <iostream>
using namespace std;

typedef long long int ll;
typedef unsigned long long int ull;

int mod;

void fill_zeros(vector< vector<ll> >& arr) {
	for (auto &v : arr) {
		fill(v.begin(), v.end(), 0);
	}
}

void print_mat(vector< vector<ll> >& arr) {
	int n = arr.size(), m = arr[0].size();
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < m; j++) {
			printf("%lld ", arr[i][j]);
		} printf("\n");
	}
}

void mul(vector< vector<ll> > &a, vector< vector<ll> > &b, vector< vector <ll> > &c) {
	int n = a.size(), m = b.size(), k = b[0].size();
	for (int i = 0; i < n; ++i) {
		for (int j = 0; j < k; ++j) {
			for (int q = 0; q < m; ++q) {
				c[i][j] = (c[i][j] + (a[i][q] * b[q][j]) % mod) % mod;
			}
		}
	}
}

vector< vector <ll> > binpow(vector< vector<ll> > &arr, ll pow) {
	int n = arr.size();
	vector < vector <ll> > res(n, vector <ll>(n));
	if (pow == 0) {
		for (int i = 0; i < n; i++) {
			res[i][i] = 1;
		}
		return res;
	}
	if (pow % 2 == 1) {
		auto b = binpow(arr, pow - 1);
		mul(b, arr, res);
	} else {
		auto b = binpow(arr, pow / 2);
		int size1 = b.size(), size2 = b[0].size();
		mul(b, b, res);
	}
	return res;
}

ll sum(vector< vector<ll> > &arr) {
	int n = arr.size(), m = arr[0].size();
	ll res = 0;
	for (int i = 0; i < n; ++i) {
		for (int j = 0; j < m; ++j) {
			res = (res + arr[i][j]) % mod;
		}
	}
	return res;
}

int main() {
	int t;
	scanf("%d", &t);

	ll n;
	int r, l;

	vector< vector <ll> > step(10, vector <ll>(10));
	vector< vector <ll> > base = {
		{0}, {1}, {1}, {1}, {1}, {1}, {1}, {1}, {1}, {1}
	};
	vector< vector <ll> > res(10, vector <ll>(1));

	while (t--) {
		fill_zeros(step);
		fill_zeros(res);
		scanf("%lld%d%d%d", &n, &l, &r, &mod); --n;
		for (int i = 0; i < 10; i++) {
			for (int d = l; d <= r; d++) {
				if (i - d >= 0) step[i][i - d] = 1;
				if (i + d < 10) step[i][i + d] = 1;
			}
		}
		auto n_steps = binpow(step, n);
		mul(n_steps, base, res);
		printf("%lld\n", sum(res));
	}
	
	return 0;
}
