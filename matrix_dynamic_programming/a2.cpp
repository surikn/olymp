#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#define N 750
using namespace std;

typedef long long int ll;

ll a[N][N], b[N][N], res[N][N];

int main() {
	ll n, m;
	scanf("%lld%lld", &n, &m);

	for (int i = 0; i < n; i++) {
		for (int j = 0; j < n; j++) {
			scanf("%lld", &a[i][j]);
		}
	}

	for (int i = 0; i < n; i++) {
		for (int j = 0; j < n; j++) {
			scanf("%lld", &b[i][j]);
		}
	}

	for (int i = 0; i < n; i++) {
		for (int j = 0; j < n; j++) {
			for (int q = 0; q < n; q++) {
				res[i][j] += (a[i][q] * b[q][j]) % m;
				res[i][j] = res[i][j] % m;
			}
			printf("%lld ", res[i][j]);
		}
		printf("\n");
	}
}
