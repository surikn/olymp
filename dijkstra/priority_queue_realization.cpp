#include <iostream>
#include <vector>
#include <queue>
#include <utility>
#include <algorithm>
using namespace std;

typedef long long ll;

void solve();

int main() {
	ios::sync_with_stdio(false);
	cin.tie(0);
	cout.tie(0);

	solve();
	return 0;
}

const int INF = 1e9;
const int N = 5000;
priority_queue <pair<int, int> > q;
vector <vector <pair<int, int> > > gr(N);

ll distances[N];
bool used[N];

void solve() {
	int n, s, f;
	cin >> n >> s >> f;
	--s; --f;

	fill(distances, distances + N, INF);

	int d;
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < n; j++) {
			cin >> d;
			if (d != -1 && i != j) {
				gr[i].push_back({ j, d });
			}
		}
	}

	q.push({0, s}); // {-dis, ver_to}
	distances[s] = 0;

	while (!q.empty()) {
		int now = q.top().second; q.pop();
		if (used[now]) continue;
		used[now] = true;
		for (auto to : gr[now]) {
			int to_ver = to.first, to_dis = to.second;
			if (distances[now] + to_dis < distances[to_ver]) {
				distances[to_ver] = distances[now] + to_dis;
				q.push({ -to_dis, to_ver });
			}
		}
	}

	cout << (distances[f] == INF ? -1 : distances[f]) << '\n';
}
